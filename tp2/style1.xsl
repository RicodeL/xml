<?xml version="1.0" encoding="UTF-8" ?>

<!-- New XSLT document created with EditiX XML Editor (http://www.editix.com) at Fri Oct 19 15:44:19 CEST 2018 -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output 
	method="html"
	encoding="UTF-8"
        	indent="yes" 
        	omit-xml-declaration="yes"
	/>
	
	<xsl:template match="/">
	<html>
		<body>
			<table>
				<xsl:apply-templates select="/breakfast_menu/food" />
			</table>
		</body>
	</html>
	</xsl:template>
	
	<xsl:template match="food">
		
   		<thead>
   			<tr bgcolor="#9acd32">
   				<td><xsl:apply-templates select="name"></xsl:apply-templates> - <xsl:apply-templates select="price"></xsl:apply-templates></td>
   			</tr>
   		</thead>
   		<tbody>
   			<tr bgcolor="#f4ede3">
   				<td><xsl:apply-templates select="description"></xsl:apply-templates></td>
   			</tr>
   		</tbody>
   	
	</xsl:template>
	
	<xsl:template match="name">
		<strong><xsl:value-of select="."/></strong>
	</xsl:template>
	
	<xsl:template match="price">
		<xsl:value-of select="."></xsl:value-of>
	</xsl:template>

</xsl:stylesheet>


