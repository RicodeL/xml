# XML TP2 : XPath

### Exercice 1

Question a

```xquery
//Composition
```

Question b

```xquery
//Composition[count(following-sibling::soloist) = 1]
```

Question c

```xquery
//performance[not(child::soloist) and count(child::orchestra) = 1]
```

Question d

```xquery
/CDlist/CD[publisher="Deutche Grammophon"]/performance[orchestra="London Symphony Orchestra"]/soloist
```

Question e

```xquery
//CD[performance/orchestra="London Symphony Orchestra"]
```

### Exercice 2

Question 1

```xquery
/liste/livre[position()=2]/*
```

Question 2

```xquery
/liste/livre[position()=1]/following-sibling::*/titre
```

Question 3

```xquery
/liste/livre[position()=2]/following-sibling::*/*
```

Question 4

```xquery
(/liste/livre/titre[@genre='jeu'])[position()=last()]
```

Question 5

```xquery
/liste/livre[parution="2006"][position()=2]/titre
```

### Exercice 3

XLST stylesheet :

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<!-- New XSLT document created with EditiX XML Editor (http://www.editix.com) at Fri Oct 19 15:44:19 CEST 2018 -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output 
	method="html"
	encoding="UTF-8"
    indent="yes" 
    omit-xml-declaration="yes"
	/>
	
	<xsl:template match="/">
	<html>
		<body>
			<table>
				<xsl:apply-templates select="/breakfast_menu/food" />
			</table>
		</body>
	</html>
	</xsl:template>
	
	<xsl:template match="food">
		
   		<thead>
   			<tr bgcolor="#9acd32">
   				<td><xsl:apply-templates select="name"></xsl:apply-templates> - <xsl:apply-templates select="price"></xsl:apply-templates></td>
   			</tr>
   		</thead>
   		<tbody>
   			<tr bgcolor="#f4ede3">
   				<td><xsl:apply-templates select="description"></xsl:apply-templates></td>
   			</tr>
   		</tbody>
   	
	</xsl:template>
	
	<xsl:template match="name">
		<strong><xsl:value-of select="."/></strong>
	</xsl:template>
	
	<xsl:template match="price">
		<xsl:value-of select="."></xsl:value-of>
	</xsl:template>

</xsl:stylesheet>
```

### Exercice 4



